const sharp = require('sharp');
const fs = require('fs');
const path = require('path');
const { program } = require('commander');

program
  .option('-pt, --path-type <string>')
  .option('-f, --from <string>')
  .option('-t, --to <string>')

program.parse();
const options = program.opts();

const inputFolder = options.pathType ? options.from : __dirname + '/startFolder';
const outputFolder = options.pathType ? options.to : __dirname + '/endFolder';

fs.readdir(inputFolder, (err: string, files: string[]) => {
  if (err) {
    console.log(err);
    return;
  }

  files.forEach((file: string) => {
    const ext = path.extname(file);
    if (ext === '.jpg' || ext === '.png') {
      const inputPath = path.join(inputFolder, file);
      const outputPath = path.join(outputFolder, path.parse(file).name + '.webp');

      sharp(inputPath)
        .webp({ quality: 80 })
        .toFile(outputPath, (err: string) => {
          if (err) {
            console.log(err);
          } else {
            console.log(`File ${file} converted to WebP format`);
          }
        });
    }
  });
});
